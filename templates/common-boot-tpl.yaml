device_type: {{device_type}}
priority: medium
{% macro kernel_type(arch) -%}
    {% if arch == 'arm64' -%}
        image
    {%- else -%}
        zimage
    {%- endif %}
{%- endmacro %}

visibility: {{visibility}}

notify:
  callback:
    url: https://lavaphabbridge.apertis.org/
    method: POST
    dataset: results
    content-type: json
    token: lava-phab-bridge
  criteria:
    status: finished

timeouts:
  job:
    minutes: 180
  action:
    minutes: 120
  connections:
    overlay-unpack:
      minutes: 20

context:
  kernel_start_message: '.*'

metadata:
  source: https://gitlab.apertis.org/infrastructure/apertis-tests
  image.version: '{{image_date}}'
  image.release: '{{release_version}}'
  image.arch: '{{arch}}'
  image.board: '{{board}}'
  image.type: '{{image_type}}'

actions:
  - deploy:
      namespace: flash
      timeout:
        minutes: 3
      to: tftp
      kernel:
        url: https://images.apertis.org/lava/nfs/{{release_version}}/{{arch}}/vmlinuz
        type: {{kernel_type(arch)}}
      nfsrootfs:
        url: https://images.apertis.org/lava/nfs/{{release_version}}/{{arch}}/apertis-nfs-{{arch}}.tar.gz
        compression: gz
      ramdisk:
        url: https://images.apertis.org/lava/nfs/{{release_version}}/{{arch}}/initrd.img
        compression: gz
      os: debian
      {% if needs_dtb -%}
      dtb:
        url: https://images.apertis.org/lava/nfs/{{release_version}}/{{arch}}/dtbs/{{dtb_root}}{{device_type}}.dtb
      {%- endif %}

  - boot:
      namespace: flash
      timeout:
        minutes: 10
      method: {{boot_method}}
      commands: nfs
      prompts:
        - 'nfsuser@apertis:'
        - '\$ '
        - '\# '
      auto_login:
        login_prompt: 'apertis-nfs login:'
        username: nfsuser
        password_prompt: 'Password:'
        password: nfsuser
        login_commands:
          - sudo su

  - deploy:
      namespace: system
      timeout:
        minutes: 60
      to: usb
      device: sd-card
      tool:
        prompts: ['copying time: [0-9ms\.\ ]+, copying speed [0-9\.]+ MiB\/sec']
      images:
        image:
          url: {{baseurl}}/{{imgpath}}/{{image_date}}/{{arch}}/{{image_type}}/{{image_name}}.img.gz
        bmap:
          url: {{baseurl}}/{{imgpath}}/{{image_date}}/{{arch}}/{{image_type}}/{{image_name}}.img.bmap
      uniquify: false
      os: ubuntu
      writer:
        tool: /usr/bin/bmaptool
        options: copy {DOWNLOAD_URL} {DEVICE}
        prompt: 'bmaptool: info'

  - boot:
      namespace: system
      timeout:
        minutes: 5
      method: {{boot_method}}
      commands: {{boot_commands}}
      transfer_overlay:
        download_command: sudo mount -o remount,rw / ; connmand-wait-online ; cd /tmp ; busybox wget
        unpack_command: sudo tar -C / -xf
      parameters:
        shutdown-message: "reboot: Restarting system"
      auto_login:
        login_prompt: 'login:'
        username: 'user'
        password_prompt: 'Password:'
        password: user
        login_commands:
          - sudo su
      prompts:
        - '\$ '
        - '/home/user #'
        - '/tmp #'

  - test:
      timeout:
        minutes: 15
      namespace: system
      name: sanity-check
      definitions:
        - repository: https://gitlab.apertis.org/tests/apertis-test-cases.git
          revision: 'apertis/v2020'
          from: git
          path: test-cases/sanity-check.yaml
          name: sanity-check
        - repository: https://gitlab.apertis.org/infrastructure/apertis-tests
          revision: 'apertis/v2020'
          from: git
          path: misc/add-repo.yaml
          name: add-repo
        - repository: https://gitlab.apertis.org/tests/apertis-test-cases.git
          revision: 'apertis/v2020'
          from: git
          path: test-cases/disk-rootfs-fsck.yaml
          name: disk-rootfs-fsck
