/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s:
 *
 * A small library that can be loaded using LD_PRELOAD to do malicious things 
 * to test whether apparmor works
 */

#define _GNU_SOURCE
#include <dlfcn.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <glib.h>

static void
do_malicious_stuff (void)
{
  const char *filename = "/etc/shadow";

  if (fopen (filename, "r") == NULL)
    {
      fprintf (stderr, "Unable to be malicious: %s -- SUCCESS\n",
               strerror(errno));
    }
  else
    {
      fprintf (stderr, "Malicious code read contents of '%s' -- FAILURE\n",
               filename);
      /* Exit immediately if apparmor doesn't stop us. */
      exit (EXIT_FAILURE);
    }
}

gboolean
g_option_context_parse            (GOptionContext      *context,
						   gint                *argc,
						   gchar             ***argv,
						   GError             **error)

{
    gboolean (*orig_f)(GOptionContext *, gint *, gchar ***, GError **)
        = dlsym (RTLD_NEXT, "g_option_context_parse");

    do_malicious_stuff ();

    orig_f (context, argc, argv, error);
}
