#!/bin/sh
# vim: set sts=4 sw=4 et tw=0 :

set -e
set -x

CURDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)

if [ $# -ne 1 ] || { [ "$1" != normal ] && [ "$1" != malicious ] ; }
then
    echo "Usage: $0 <normal|malicious>"
    exit 1
fi

# This expects to run as root.
if [ "$(id -u)" -ne 0 ]; then
    echo "$CURDIR/ofonod must run as root."
    exit 2
fi

systemctl stop ofono.service

if [ "$1" = "malicious" ]; then
    # Create temporary service conf file
    SERVICE_CONF_DIR="/run/systemd/system/ofono.service.d/"
    mkdir -p $SERVICE_CONF_DIR

    SERVICE_CONF_FILE=$SERVICE_CONF_DIR"apertis-tests-temporary.conf"
    echo "[Service]" > $SERVICE_CONF_FILE
    echo "Environment=LD_PRELOAD=$CURDIR/libofonod-malicious-override.so" >> $SERVICE_CONF_FILE

    # Load this temp file
    systemctl daemon-reload
fi

systemctl start ofono.service

# Check it is actually confined.
pid=`pidof /usr/sbin/ofonod`
confinement=`cat /proc/$pid/attr/current`
if [ "$confinement" != "/usr/sbin/ofonod (enforce)" ]; then
    echo "Invalid confinement: $confinement"
    exit 3
fi

sleep 5

systemctl stop ofono.service

if [ -n "$SERVICE_CONF_FILE" ]; then
    rm $SERVICE_CONF_FILE
    # Reload now that the temp config file has been removed
    systemctl daemon-reload
fi

systemctl start ofono.service

# Lava compatibility:
# systemctl status will exit 0 if service is running, non-zero otherwise
systemctl status ofono.service
exit $?
