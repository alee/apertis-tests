/* vim: set sts=4 sw=4 et :
 *
 * A small library that can be loaded using LD_PRELOAD to do malicious things 
 * to test whether apparmor works
 */

/* Easier for the build system */
#include "../common/function-malicious-override.c"

void
tracker_db_manager_shutdown (void)
{
    void (*orig_f)(void) = dlsym (RTLD_NEXT, "tracker_db_manager_shutdown");

    do_malicious_stuff ();

    orig_f ();
}
