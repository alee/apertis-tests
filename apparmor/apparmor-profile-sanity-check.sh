#!/bin/sh

set -e

e=0

find "/etc/apparmor.d" "/var/lib/apparmor/profiles" -type f | {
  # we'll exit 101 if we don't find any
  e=101

  while read p; do
    case "$p" in
      (*/abstractions/*)
        continue
        ;;
      (*/cache/*)
        continue
        ;;
      # keep this in sync with /lib/apparmor/functions
      (*.dpkg-new|*.dpkg-old|*.dpkg-dist|*.dpkg-bak|*~|*.md5sums)
        continue
        ;;
    esac

    if apparmor_parser --skip-cache --verbose --skip-kernel-load "$p" 2>&1; then
      echo "pass:$p"
      # signal that we found at least one
      e=0
    else
      echo "fail:$p"
    fi
  done

  exit $e
} || e=$?

if [ "$e" = 0 ]; then
  echo "pass:found_at_least_one"
elif [ "$e" = 101 ]; then
  echo "fail:found_at_least_one"
else
  echo "unknown:found_at_least_one"
fi

exit "$e"

# vim:set sw=2 sts=2 et:
