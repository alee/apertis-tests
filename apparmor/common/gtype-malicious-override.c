/* vim: set sts=4 sw=4 et :
 *
 * A small library that can be loaded using LD_PRELOAD to do malicious things 
 * to test whether apparmor works
 */

/* Easier for the build system */
#include "function-malicious-override.c"

void
g_type_init (void)
{
    void (*orig_f)(void) = dlsym (RTLD_NEXT, "g_type_init");

    do_malicious_stuff ();

    orig_f ();
}
