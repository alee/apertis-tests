all:

pkglibdir = /usr/lib/apertis-tests

SUBDIRS = \
	apparmor/goals \
	apparmor/geoclue \
	apparmor/tracker \
	apparmor/ofono \
	bluez/ \
	boot-performance/automated/ \
	clutter-bigger-reactive-area/ \
	clutter-i18n/manual/ \
	clutter-rotate/ \
	clutter-transparent-stage/ \
	clutter-zoom/ \
	common/ \
	dbus/ \
	gstreamer-buffering/automated/ \
	networking/proxy-manual/ \
	traffic-control/manual/ \
	sdk/automated/ \
	connman \
	contacts \
	gstreamer-viv-direct-texture/automated/ \
	telepathy \
	$(NULL)

# Bits that are architecture-independent and can just be copied
COPY = \
	$(wildcard apparmor/*.sh) \
	$(wildcard apparmor/*.yaml) \
	apertis_tests_lib \
	apparmor/run-aa-test \
	common \
	dbus \
	folks \
	geoclue/automated \
	grilo \
	gstreamer-decode \
	inherit-config.sh \
	misc \
	predeployed-misc \
	resources \
	sdk \
	templates \
	tracker \
	$(NULL)

all:
	for path in $(SUBDIRS); do \
		if test -x $$path/autogen.sh; then \
			( cd $$path && ./autogen.sh --prefix=/usr ) || exit $$?; \
		fi; \
		$(MAKE) -C $$path all || exit $$?; \
	done

$(patsubst %,install-%,$(COPY)): install-%:
	install -d $(DESTDIR)$(dir $(pkglibdir)/$*)
	cp -a $* $(DESTDIR)$(dir $(pkglibdir)/$*)

install: $(patsubst %,install-%,$(COPY))
	install -d $(DESTDIR)$(pkglibdir)
	install -d $(DESTDIR)$(pkglibdir)/apparmor
	install -d $(DESTDIR)/usr/lib/chaiwala-apparmor-tests
	for path in $(SUBDIRS); do $(MAKE) -C $$path install || exit $$?; done

clean:
	for path in $(SUBDIRS); do $(MAKE) -C $$path clean || exit $$?; done

check:
	misc/syntax-check.py
