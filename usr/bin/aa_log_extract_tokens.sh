#!/bin/sh
# vim: set sts=4 sw=4 et tw=80 :

usage() {
    echo "Usage: $0 <event mode> [event mode]..."
    echo "And pipe logs to it"
    echo "Valid values for [event mode] are: ALLOWED, DENIED, AUDIT"
}

events=""
while [ $# -gt 0 ] ; do
	case $1 in
		AUDIT|ALLOWED|DENIED)
			events="$events $1"
			;;
		*)
			echo Unknow parameter $1
			echo
			usage
			exit 1
			;;
	esac
	shift
done

# Pass in audit string, returns formatted block pretty similat as
# `aa_log_extract_tokens.pl` would have provided. Need to return:
#
# profile:/usr/bin/ribchester
# apparmor:AUDIT
# operation:exec
# name:/bin/cp
# requested_mask:x
# info:ix fallback
# target:/usr/bin/ribchester
#
# profile:/usr/bin/busctl
# apparmor:DENIED
# denied_mask:r
# operation:open
# name:/proc/879/stat
# requested_mask:r
#
# Note: apparmor uses the term "DENIED" rather than "REJECTING"
awk -v events="$events" '
	BEGIN {
		# Get requested event modes
		split(events, event_modes, " ")
		# List of entries to return
		split("profile apparmor denied_mask operation name requested_mask info target ", entries, " ")
	}
	{
		# Parse log entry
		for (i = 1; i <= NF; i++) {
			n = index($i, "=");
			if(n) {
				key = substr($i, 1, n - 1)
				value = substr($i, n + 1)
				# awk split stdin on space, recreate quoted strings
				if (match(value, "^\".*") && !match(value, "^\".*\"")) {
					i++
					while (!match($i, ".*\"")) {
						value = value " " $i
						i++
					}
					value = value " " $i
				}
				# Strip quotes
				gsub("\"","",value)
				vars[key] = value
			}
		}
	}
	{
		for (x in event_modes) {
			if (vars["apparmor"] == event_modes[x]) {
				print "===="
				for (key in entries) {
					if (entries[key] in vars)
						print entries[key] ":" vars[entries[key]]
				}
			}
		}
		delete vars
	}
'
