#!/bin/sh
#
# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# ade-tests-custom.sh
#
# This test script will exercise some ade features in order to catch issues in what real users may try out:
# * listing installed sysroots
# * listing available sysroots
# * downloading sysroots
# * verifying sysroots
# * installing sysroots
# * targeting different distributions, releases and architectures
# * cross-building
# * exporting app-bundles
# * default and non-default parameters
#
# It needs:
# * network access to fetch sysroots for arm64 and armhf
# * network access to clone the sample-app source repository
# * the arm64 gcc-aarch64-linux-gnu cross-toolchain
# * the armhf gcc-arm-linux-gnueabi cross-toolchain

set -eux

. /etc/os-release # import $ID and $VERSION_ID

LANG=C.UTF-8
DISTRIBUTION=$ID
RELEASE=$VERSION_ID
OLDRELEASE=18.06
ARCH=arm64 # the non-default one to be tested
SYSROOTINDEX="https://images.apertis.org/sysroot/$OLDRELEASE/sysroot-$DISTRIBUTION-$OLDRELEASE-$ARCH"
SAMPLEAGENTREPO=https://gitlab.apertis.org/sample-applications/helloworld-simple-agent/

git clone $SAMPLEAGENTREPO sample-app

ade sysroot list

echo ==== Testing ade usage with custom parameters

ade sysroot installed --distro $DISTRIBUTION --release $OLDRELEASE --arch $ARCH

ade sysroot latest --distro $DISTRIBUTION --release $OLDRELEASE --arch $ARCH

ade sysroot latest --distro $DISTRIBUTION --release $OLDRELEASE --arch $ARCH --url "$SYSROOTINDEX"

ade sysroot download --distro $DISTRIBUTION --release $OLDRELEASE --arch $ARCH --url "$SYSROOTINDEX" --dest /opt/sysroot/downloads/

ade sysroot verify --file /opt/sysroot/downloads/sysroot-$DISTRIBUTION-$OLDRELEASE-${ARCH}_*.tar.gz

ade sysroot install --file /opt/sysroot/downloads/sysroot-$DISTRIBUTION-$OLDRELEASE-${ARCH}_*.tar.gz

ade sysroot list

cd sample-app

ade configure --debug --sysroot $DISTRIBUTION-$OLDRELEASE-$ARCH -- --disable-Werror
ade export

stat *.bundle

test "$(stat -c %s *.bundle)" -gt 0

rm -r ../sample-app
