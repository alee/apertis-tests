# -*- coding: utf-8 -*-

# Copyright © 2015 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import unittest
import shutil
import os

from gi.repository import GLib

MEDIADIR = '/usr/lib/apertis-tests/resources/media'

# define this here to make lint happy with too long lines
LONG_JPEG_NAME = '320px-European_Common_Frog_Rana_temporaria.jpg'


class ApertisTest(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.loop = GLib.MainLoop.new(None, False)
        self.homedir = os.path.expanduser("~")

    def copy_medias(self, dst):
        """
        Copy media resources from apertis-tests to @dst with per media type
        subdirectory corresponding to the default subdirectories in $HOME.
        """
        print("ApertisTest: copying medias to " + dst)
        self.__copytree(MEDIADIR + '/audio', dst + '/Music')
        self.__copytree(MEDIADIR + '/documents', dst + '/Documents')
        self.__copytree(MEDIADIR + '/images', dst + '/Pictures')
        self.__copytree(MEDIADIR + '/playlists', dst + '/Music')
        self.__copytree(MEDIADIR + '/videos', dst + '/Videos')

    def __copytree(self, root_src_dir, root_dst_dir):
        # This is our own version of shutil.copytree. The difference is that
        # if the destination directory already exists it adds files into it. If
        # files already exists they are overwritten.
        for src_dir, dirs, files in os.walk(root_src_dir):
            dst_dir = src_dir.replace(root_src_dir, root_dst_dir)
            if not os.path.exists(dst_dir):
                os.mkdir(dst_dir)
            for file_ in files:
                src_file = os.path.join(src_dir, file_)
                dst_file = os.path.join(dst_dir, file_)
                if os.path.exists(dst_file):
                    os.remove(dst_file)
                shutil.copy2(src_file, dst_dir)
