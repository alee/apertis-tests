# -*- coding: utf-8 -*-

# Copyright © 2015 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from gi.repository import Grl
from gi.repository import GrlPls


class GriloBrowserMixin:
    def browse(self, source, prefix, pls=None):
        self.source = source
        self.prefix = 'file://%s/' % prefix
        self.medias = {}

        ops = self.source.supported_operations()
        self.assertTrue((ops & Grl.SupportedOps.BROWSE) != 0)

        self.__browse_recurse(None, pls)

        return self.medias

    def __browse_recurse(self, media, pls=None):
        caps = self.source.get_caps(Grl.SupportedOps.BROWSE)
        options = Grl.OperationOptions.new(caps)
        options.set_resolution_flags(Grl.ResolutionFlags.IDLE_RELAY)

        keys = [Grl.METADATA_KEY_TITLE,
                Grl.METADATA_KEY_ALBUM,
                Grl.METADATA_KEY_ARTIST,
                Grl.METADATA_KEY_THUMBNAIL,
                Grl.METADATA_KEY_URL,
                Grl.METADATA_KEY_CHILDCOUNT]

        if pls is not None:
            medias = GrlPls.browse_sync(self.source, pls, keys, options, None)
        else:
            medias = self.source.browse_sync(media, keys, options)

        for media in medias:
            if media.is_container():
                self.__browse_recurse(media)
                continue

            url = media.get_url()
            if url.startswith(self.prefix):
                suffix = url[len(self.prefix):]

                print("GriloBrowserMixin: add media", suffix)
                self.medias[suffix] = media
