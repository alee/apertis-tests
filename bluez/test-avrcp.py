#! /usr/bin/python3
# -*- coding: utf-8 -*-
#
# BlueZ - Bluetooth protocol stack for Linux
#
# Copyright © 2012, 2015 Collabora Ltd.
#
# SPDX-License-Identifier: GPL-2.0+
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import time
import sys
import dbus
from optparse import OptionParser, make_option
import os

# import from toplevel directory
sys.path.insert(0, os.path.join(os.path.dirname(__file__), os.pardir))
from apertis_tests_lib.bluez import AskAgent
from apertis_tests_lib.bluez import build_device_path
from apertis_tests_lib.bluez import get_hci


bus = dbus.SystemBus()

manager = dbus.Interface(bus.get_object("org.bluez", "/"),
                         "org.freedesktop.DBus.ObjectManager")

option_list = [
    make_option("-i", "--device", action="store",
                type="string", dest="dev_id"),
]

parser = OptionParser(option_list=option_list)

(options, args) = parser.parse_args()

objs = manager.GetManagedObjects()
adapters = {p: i for p, i in objs.items()
            if 'org.bluez.Adapter1' in i.keys()}

if options.dev_id:
    adapter_path = '/org/bluez/' + options.dev_id
else:
    adapter_path = list(adapters.keys())[0]

adapter = dbus.Interface(bus.get_object("org.bluez", adapter_path),
                         "org.bluez.Adapter1")

if len(args) < 1:
    print("""Usage: %s [-i device] <bdaddr>""" % sys.argv[0])
    sys.exit(1)

device = build_device_path(get_hci(adapter_path), args[0])
audio = dbus.Interface(bus.get_object("org.bluez", device),
                       "org.bluez.AudioSource")

# Connect all profiles
connected = audio.Get('org.bluez.Device1', 'Connected',
                      dbus_interface='org.freedesktop.DBus.Properties')
if not connected:
    audio.Connect(dbus_interface='org.bluez.Device1')

time.sleep(5)

control = dbus.Interface(bus.get_object("org.bluez", device),
                         "org.bluez.MediaControl1")
control.VolumeDown()
control.VolumeUp()
