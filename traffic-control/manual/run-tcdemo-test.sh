#!/bin/sh
# vim: set sts=4 sw=4 et tw=0 :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

# Initialization and setup shared between the test scripts
. "${TESTDIR}/init-setup.sh"
setup_success

###########
# Execute #
###########

_setup_background_download() {
    # This function is executed in a subshell. The main shell needs to be
    # able to kill it together with wget but bash interprets signals after
    # a command complete. Therefore, wget is ran in background with a wait.
    trap "kill \$WGET_PID; exit 0" SIGTERM
    while true ; do
        wget --progress=dot $URL -O /dev/null > /dev/null 2>&1 &
        WGET_PID=$!
        wait
    done
}

test_tcdemo() {
    local tc=$1
    local background=$2
    local ret
    local server_pid
    local port="9000"

    ## Setup ##
    sudo $TCMMD --save-stats=$TCMMD_LOG.$tc.$background -i $NET_INTERFACE >> $TCMMD_LOG 2>&1 &
    TCMMD_PID=$!
    ROOT_PROC_KILL_LIST="${ROOT_PROC_KILL_LIST} ${TCMMD_PID}"

    if [ "$background" = "background" ] ; then
      # aggressive downloads: 8 in parallel
      for i in $(seq 1 8) ; do
        _setup_background_download &
        DOWNLOAD_PID=$!
        PROC_KILL_LIST="${PROC_KILL_LIST} ${DOWNLOAD_PID}"
      done
      sleep 1
    fi

    TCDEMO_OPTS=
    if [ "$tc" = "notc" ] ; then
      TCDEMO_OPTS=--disable-tc
    fi
    ${TCDEMO} ${TCDEMO_OPTS} "$URL" > ${TCDEMO_LOG}.$tc.$background
    ret=$?
    if [ $ret -gt 0 ]; then
        cry "Test failed!"
        _cleanup
        return $ret
    fi

    BUFFER_CRITICALLY_LOW_COUNT=$(grep ^buffer_critically_low_count ${TCDEMO_LOG}.$tc.$background | sed s/buffer_critically_low_count=//)

    say "$tc $background: buffer_critically_low_count = $BUFFER_CRITICALLY_LOW_COUNT"

    ## Cleanup ##
    _cleanup
    return $ret
}

cleanup_and_maybe_fail () {
    s=$?
    _run_cmd _cleanup
    [ $s -eq 0 ] || test_failure
}
trap "cleanup_and_maybe_fail" EXIT

title "Test without traffic-control support, without background traffic (should not freeze)"
test_tcdemo notc nobackground
if [ $? != 0 ] ; then
  test_failure
fi

title "Test with traffic-control support, without background traffic (should not freeze)"
test_tcdemo tc nobackground
if [ $? != 0 ] ; then
  test_failure
fi

title "Test without traffic-control support, with background traffic (should freeze)"
test_tcdemo notc background
if [ $? != 0 ] ; then
  test_failure
fi

title "Test with traffic-control support, with background traffic (should not freeze)"
test_tcdemo tc background
if [ $? != 0 ] ; then
  test_failure
fi

test_success
