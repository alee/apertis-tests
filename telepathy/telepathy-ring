#!/usr/bin/python

import sys
import time
import os
import threading
from gi.repository import TelepathyGLib
from gi.repository import GObject, GLib, Gio
Tp = TelepathyGLib

def usage():
    print "%s [sms|call] CONTACT" % sys.argv[0]
    print "CONTACT is the contact's number"

    exit (-1)

class RingTest:
    def __init__ (self, account_id, contact_id, main_loop):
        self.success = False
        self.account_id = account_id
        self.contact_id = contact_id
        self.main_loop = main_loop

        # Setup account
        mgr = Tp.AccountManager.dup()
        self.account = mgr.ensure_account ("%s%s" % (Tp.ACCOUNT_OBJECT_PATH_BASE, account_id))
        self.connection_notify_id = self.account.connect ("notify::connection", self.connection_cb, None)
        self.connection_status_notify_id = self.account.connect ("notify::connection-status", self.connection_cb, None)

        # Enable & Request availability, causing our connection to be setup
        self.account.set_enabled_async (True, None, None)
        self.account.request_presence_async (Tp.ConnectionPresenceType.AVAILABLE, '', '', None, None)

    def contact_list_state_cb (self, connection, spec, data):
        print "contact_list_state_cb"

        if connection.get_contact_list_state () == Tp.ContactListState.SUCCESS:
            self.start_test ()

    def connection_prepared_cb (self, c, r, u):
        c.prepare_finish (r)
        self.connection.dup_contact_by_id_async (
            self.contact_id,
            [Tp.ContactFeature.SUBSCRIPTION_STATES,
             Tp.ContactFeature.CAPABILITIES],
            self.got_contact, None)

    def start_test (self):
        print "start_test"

        self.connection.prepare_async (None, self.connection_prepared_cb, None)

    def connection_cb (self, account, spec, data):
        print "connection_cb"

        s = self.account.get_property ("connection-status")
        c = self.account.get_property ("connection")
        if s == Tp.ConnectionStatus.CONNECTED and c != None:
            self.account.disconnect (self.connection_notify_id)
            self.account.disconnect (self.connection_status_notify_id)

            self.connection = c
            if c.has_interface (Tp.IFACE_CONNECTION_INTERFACE_CONTACT_LIST):
                c.connect ("notify_contact-list-state", self.contact_list_state_cb, None)
                if c.get_contact_list_state () == Tp.ContactListState.SUCCESS:
                    self.start_test ()
            else:
                self.start_test ()

    def create_channel_finished_cb(self, req, result, u):
        print "create_channel_finished_cb"
        print result

        res = req.ensure_and_handle_channel_finish (result)
        print res
        (self.channel, context) = res
        print 'chan path :'+ self.channel.get_object_path ()
        self.channel_ready ()

    def create_request(self):
        raise NotImplemented('implement create_request')

    def handle_capabilities_cb (self, contact, spec, data):
        print "handle_capabilities_cb"

        request = self.create_request ()
        request.ensure_and_handle_channel_async (None, self.create_channel_finished_cb, None)

    def handle_contact_states_cb (self, contact, spec, data):
        print "handle_contact_states_cb"

        p = self.contact.get_publish_state ()
        s = self.contact.get_subscribe_state ()
        if p == Tp.SubscriptionState.YES and s == Tp.SubscriptionState.YES:
            print "Subscription complete"
        elif p == Tp.SubscriptionState.ASK:
            self.contact.authorize_publication_async (
                lambda c, r, u: c.authorize_publication_finish(r), None)

    def got_contact (self, c, r, u):
        print "got_contact"

        self.contact = c.dup_contact_by_id_finish (r)

        if self.connection.has_interface (Tp.IFACE_CONNECTION_INTERFACE_CONTACT_LIST):
            s = self.contact.get_subscribe_state ()

            if s != Tp.SubscriptionState.YES:
                print "Asking for subscription"
                self.contact.request_subscription_async (
                    'Test subscription',
                    lambda c, r, u: c.request_subscription_finish(r),
                    None)
                self.contact.connect ("notify::publish-state", self.handle_contact_states_cb, None)
                self.contact.connect ("notify::subscribe-state", self.handle_contact_states_cb, None)

        self.contact.connect ("notify::capabilities", self.handle_capabilities_cb, None)
        self.handle_capabilities_cb (self.contact, None, None)
        self.handle_contact_states_cb (self.contact, None, None)

# Test case for making calls
class RingCallTest(RingTest):
    def __init__(self, account_id, contact_id, main_loop):
        RingTest.__init__ (self, account_id, contact_id, main_loop)
        self.proxy = None

    def create_request (self):
        print "create_request"

        request = Tp.AccountChannelRequest.new_audio_call (self.account, 0)
        request.set_hint ('call-mode', GLib.Variant('s', 'test-inputs'))
        request.set_target_contact (self.contact)

        return request

    def hangup (self):
        print "hangup"

        self.channel.hangup_async (Tp.CallStateChangeReason.USER_REQUESTED, '', '', None, None)
        self.success = True

        self.main_loop.quit ()

    def check_channel_state (self):
        print "check_channel_state"

        (state, flags, details, reason) = self.channel.get_state ()

        if state == Tp.CallState.ACTIVE:
            print "Successful call, letting it run for 5 seconds"
            GLib.timeout_add_seconds (5, self.hangup)

            if state == Tp.CallState.ENDED:
                self.success = True
                self.main_loop.quit ()

    def state_changed_cb (self, channel, spec, data):
        print "state_changed_cb"

        self.check_channel_state ()

    def call_accepted_cb (self, channel, result, u):
        print "call_accepted_cb"

        try:
            self.channel.accept_finish (result)
            self.check_channel_state ()
        except Exception, e:
            print 'error : ', e
            self.main_loop.quit ()

    def channel_ready (self):
        print "channel_ready"

        self.channel.connect ("notify::state", self.state_changed_cb, None)
        self.channel.accept_async (self.call_accepted_cb, None)

# Test case for sending sms'
class RingSmsTest(RingTest):
    def __init__(self, account_id, contact_id, main_loop):
        RingTest.__init__ (self, account_id, contact_id, main_loop)

    def create_request (self):
        print "create_request"

        request = Tp.AccountChannelRequest.new_text (self.account, 0)
        request.set_target_contact (self.contact)

        return request

    def message_sent_cb (self, channel, result, data):
        print 'message_sent_cb'
        try :
            print result
            channel.send_message_finish (result)
            self.success = True
        except Exception, e:
            print 'error : ', e

        self.main_loop.quit ()

    def channel_ready (self):
        print "channel_ready"

        msg = Tp.ClientMessage.new_text (Tp.ChannelTextMessageType.NORMAL, "Hello from tp-ring")
        self.channel.send_message_async (msg, 0, self.message_sent_cb, None)

if __name__ == '__main__':
    TelepathyGLib.debug_set_flags("all")
    if len(sys.argv) != 3:
        usage()

    _, action, contact_id = sys.argv
    main_loop = GObject.MainLoop ()

    try:
        a = os.popen ("mc-tool list | grep ring").read()
        b = a.splitlines ()
        account_id = b[0]

        if action == "sms":
            test = RingSmsTest (account_id, contact_id, main_loop)
        elif action == "call":
            test = RingCallTest (account_id, contact_id, main_loop)
        else:
            usage ()

        main_loop.run()

        if test.success:
            print "telepathy-ring: PASSED"
        else:
            print "telepathy-ring: FAILED"
    except Exception, e:
        print 'error',e
        main_loop.quit ()

        exit (-1)
