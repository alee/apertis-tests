#!/bin/sh
# vim: set sts=4 sw=4 et tw=0 :

echo "# $0: running as: $(id)"
echo "# $0: running in: $(pwd)"
echo "# $0: initial environment:"
env | LC_ALL=C sort | sed -e 's/^/#  /'

# Debuggability
set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

#########
# Setup #
#########
trap "setup_failure" EXIT

ensure_dbus_session

setup_success

###########
# Execute #
###########
test_geoclue_manual_address() {
    set -x
    local cmd addr
    cmd="/usr/lib/geoclue-2.0/demos/where-am-i"
    addr="org.freedesktop.GeoClue2"

    # Manual provider doesn't update the address without this
    pkill 'geoclue-.+'

    # Launch geoclue manually to allow LD_PRELOAD to work for 
    # chaiwala-apparmor-geoclue-tests
    /usr/lib/geoclue-2.0/geoclue

    # FIXME: https://bugzilla.gnome.org/show_bug.cgi?id=745971
    # Wait for the name to be available (ready) on the bus
    mc-wait-for-name "${addr}"

    # Unset LD_PRELOAD for this bit
    LD_PRELOAD= "${cmd}"
    ret=$?

    pkill 'geoclue-.+'
    set +x
    return $ret
}

trap "test_failure" EXIT

src_test_pass <<-EOF
test_geoclue_manual_address
EOF

test_success
