#! /usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2015 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import unittest
import sys
import os

from gi.repository import GLib
from gi.repository import Gio

# import from toplevel directory
sys.path.insert(0, os.path.join(os.path.dirname(__file__),
                                os.pardir, os.pardir))
from apertis_tests_lib.tracker import TrackerIndexerMixin
from apertis_tests_lib.tumbler import TumblerMixin
from apertis_tests_lib import ApertisTest
from apertis_tests_lib import LONG_JPEG_NAME


class TestRemovableDevice(ApertisTest, TrackerIndexerMixin, TumblerMixin):
    def __init__(self, *args, **kwargs):
        ApertisTest.__init__(self, *args, **kwargs)
        TrackerIndexerMixin.__init__(self)
        TumblerMixin.__init__(self)

    def setUp(self):
        self.monitor = Gio.VolumeMonitor.get()
        self.monitor.connect('mount-added', self.mount_added_cb)

    def tearDown(self):
        # Keep everything in place for further manual checks
        pass

    def mount_added_cb(self, monitor, mount):
        self.path = mount.get_root().get_path()
        self.loop.quit()

    def assert_has_thumbnail(self, filename):
        self.tumbler_assert_thumbnailed(self.path, filename)

    def test_removable_device(self):
        print('Please insert storage ...')
        self.loop.run()

        # Copy our medias and start indexing
        self.copy_medias(self.path)
        self.start()
        self.assert_all_indexed(self.path)

        self.tumbler_drain_queue()
        self.assert_has_thumbnail('Documents/lorem_presentation.odp')
        self.assert_has_thumbnail('Documents/lorem_spreadsheet.ods')
        self.assert_has_thumbnail('Documents/more_lorem_ipsum.odt')
        self.assert_has_thumbnail('Pictures/' + LONG_JPEG_NAME)
        self.assert_has_thumbnail('Pictures/collabora-logo-big.png')
        self.assert_has_thumbnail('Videos/big_buck_bunny_smaller.ogv')

if __name__ == "__main__":
    unittest.main()
