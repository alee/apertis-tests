#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import json

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GLib
from gi.repository import Gst

class DecodeTest(object):

    def __init__(self, path, filename, has_video):
        Gst.init(None)
        self.loop = GLib.MainLoop()
        self.filename = filename
        self.pipeline = Gst.ElementFactory.make("playbin", None)
        self.pipeline.get_bus().add_watch(GLib.PRIORITY_DEFAULT, self.async_handler, None)

        self.pipeline.set_property ("uri",
            "file://" + os.path.join(path, filename))

        self.asink = Gst.ElementFactory.make("fakesink", "asink")
        self.asink.set_property("enable-last-sample", True)
        self.pipeline.set_property("audio-sink", self.asink)

        if (has_video):
            self.vsink = Gst.ElementFactory.make("fakesink", "vsink")
            self.vsink.set_property("enable-last-sample", True)
            self.pipeline.set_property("video-sink", self.vsink)
        else:
            self.vsink = None

        self.has_video = has_video
        self.failed = False

    def async_handler(self, bus, message, data):
        if message.type == Gst.MessageType.ERROR:
            print("%s: FAILED (%s)\n" % (self.filename, message.parse_error()[1]))
            self.failed = True
            self.loop.quit()
        elif message.type == Gst.MessageType.EOS:
            self.loop.quit()
        return True

    def run_test(self):
        self.pipeline.set_state(Gst.State.PLAYING)
        self.loop.run()
        if self.failed:
            return False

        print("TEST %s - audio: %s" % (self.filename,
            self.asink.get_property("last-sample") and "PASSED" or "FAILED"))
        if self.has_video:
            print("TEST %s - video: %s" % (self.filename,
                self.vsink.get_property("last-sample") and "PASSED" or "FAILED"))
        return not self.failed


if __name__ == '__main__':
    media = json.loads(open(os.path.join(sys.argv[1], "media.js")).read())

    for x in media["files"]:
        DecodeTest(sys.argv[1], x["name"], x["video"]).run_test()
