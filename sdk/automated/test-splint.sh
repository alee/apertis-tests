#!/bin/bash

# This smoke-checks splint by running it on a simple source file and ensuring
# that the splint output matches some basic assumptions

TEST_DIR=$(dirname $0)
TEST_PROG=$(mktemp)
TEST_SRC_NAME=linty


die() {
        if [ "x$1" != "x" ]; then
                echo -e "$1"
        fi
        echo "sdk-code-analysis-tools-splint-smoke-test: FAILED"
        exit 0
}

#
# Main test
#
SPLINT_ARRAY_EXPECTED=(
"static function func_unused_static declared but not used"
"dereference of possibly null pointer null_arg"
)

splint_array_missing=()
splint_output="$(splint -strict $TEST_DIR/$TEST_SRC_NAME.c 2>&1)"

# check that every expected line is present in the splint output
matches=0
for i in $(seq 0 $((${#SPLINT_ARRAY_EXPECTED[@]} - 1))); do
        if [ "x$(echo "$splint_output" | grep -i "${SPLINT_ARRAY_EXPECTED[$i]}" )" != "x" ]; then
                matches=$(($matches + 1))
                continue
        fi

        splint_array_missing+=("${SPLINT_ARRAY_EXPECTED[$i]}")
done

STATUS_STR="FAILED"
if [ $matches -eq ${#SPLINT_ARRAY_EXPECTED[@]} ]; then
        STATUS_STR="PASSED"
else
        for n in ${splint_array_missing[@]}; do
                missing="$missing\n$n"
        done
        die "Expected patterns missing in splint output:\n$missing\n"
fi

rm -f ${TEST_PROG}

echo "sdk-code-analysis-tools-splint-smoke-test: $STATUS_STR"
