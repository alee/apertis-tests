#!/bin/sh

# This builds a program with gcov support, runs it multiple ways, and ensures
# that coverage meets expectations
WORK_DIR="$(pwd)"
TEST_DIR="$WORK_DIR/$(dirname $0)"
TEST_PROG=$(mktemp)
TEST_SRC_NAME=multipath
TEST_BUILD_DIR=$(mktemp -d)
TEST_PROG_ARGS=""
CFLAGS="-fprofile-arcs -ftest-coverage"
MIN_PORTION="1.0"

die() {
        echo $1
        exit 0
}

# Build and find gcov files inside temporary directory
cd $TEST_BUILD_DIR

#
# Build program with gcov support
#
cc $CFLAGS -o $TEST_PROG $TEST_DIR/$TEST_SRC_NAME.c

#
# Main test
#

# run the program multiple ways to ensure adequate coverage
$TEST_PROG >/dev/null 2>&1
$TEST_PROG -2 >/dev/null 2>&1
$TEST_PROG --invalid-option >/dev/null 2>&1

test_portion="$(gcov -o $(pwd) $TEST_DIR/$TEST_SRC_NAME.c | \
        grep "Lines executed" | \
        sed 's/.*:\([0-9.]*\)%.*/\1/')"

# Convert each to a percentage and drop any fractional part (so we can compare
# with [)
TEST_PCT=$(echo $test_portion | awk '{print int($1)}')
MIN_PCT=$(echo $MIN_PORTION 100 | awk '{print int($1*$2)}')

STATUS_STR="FAILED"
if [ $TEST_PCT -ge $MIN_PCT ]; then
        STATUS_STR="PASSED"
fi

# Clean temporary program and files
cd $WORK_DIR
rm -rf $TEST_PROG $TEST_BUILD_DIR

echo "sdk-code-analysis-tools-gcov-smoke-test: $STATUS_STR"
echo "Program ran in $TEST_PCT% of samples (>= $MIN_PCT% required)"
