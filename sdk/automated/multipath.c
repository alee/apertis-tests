/*
 * A trivial program which can be used to easily traverse different branches
 * (for code coverage testing).
 */

#include <stdlib.h>
#include <stdio.h>

#define PRINT_FUNC_NAME printf("In %s()\n", __func__);

static void
func_default()
{
  PRINT_FUNC_NAME
}

static void
func_2()
{
  PRINT_FUNC_NAME
}

int
main(int argc, char* argv[])
{
  if(argc >= 2 && argv[1] != NULL) {
    if(strcmp (argv[1], "-2") == 0) {
      func_2();
    } else {
      fprintf(stderr, "A program which takes a given numbered branch "
          "(for code coverage)\n\n",
          "    usage: %s [-2]\n", argv[0]);
    }
  } else {
    func_default();
  }

  return 0;
}
