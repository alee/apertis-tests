#!/bin/bash

# This smoke-checks gdb by settin a breakpoint in a trivial program and checking
# that the backtrace matches expectations

TEST_DIR=$(dirname $0)
TEST_PROG=$(mktemp)
TEST_SRC_NAME=backtrace
GDB_SCRIPT=$TEST_SRC_NAME.gdb

die() {
        echo $1
        exit 0
}

#
# Build program with gprof support
#
cc -o $TEST_PROG $TEST_DIR/$TEST_SRC_NAME.c

#
# Main test
#
BT_ARRAY_EXPECTED=(
'^#0.*func_break ()'
'^#1.*func_1 ()'
'^#2.*main ()'
)

# break tokens at newlines, not just any whitespace (so we can parse gdb output
# on a line-by-line basis)
ifs_old="$IFS"
IFS=$'\n'

bt_array_actual=($(gdb -batch -x $TEST_DIR/$GDB_SCRIPT $TEST_PROG | grep '^#'))

# check that every array of the backtrace match expectations
match=1
for i in $(seq 0 $((${#BT_ARRAY_EXPECTED[@]} - 1))); do
        if [ "$(echo "${bt_array_actual[$i]}" | grep "${BT_ARRAY_EXPECTED[$i]}" )" = "" ]; then
                match=0
        fi
done

IFS="$ifs_old"

STATUS_STR="FAILED"
if [ $match -eq 1 ]; then
        STATUS_STR="PASSED"
fi

# Clean temporary program
rm -f $TEST_PROG

echo "sdk-debug-tools-gdb-smoke-test: $STATUS_STR"
