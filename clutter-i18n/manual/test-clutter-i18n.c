#include <stdlib.h>
#include <locale.h>
#include <glib/gi18n.h>
#include <clutter/clutter.h>

#define STAGE_WIDTH 800
#define STAGE_HEIGHT 550

#undef _
#define _(str) g_dgettext (GETTEXT_PACKAGE, str)

int main(int argc, char *argv[])
{
  ClutterActor *stage, *label;

  if (setlocale (LC_ALL, "") == NULL)
    g_warning ("Locale not understood by C library, internationalization will not work\n");
  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");

  if (clutter_init (&argc, &argv) != CLUTTER_INIT_SUCCESS)
    return EXIT_FAILURE;

  stage = clutter_stage_new ();
  g_signal_connect (stage, "destroy", G_CALLBACK (clutter_main_quit), NULL);
  clutter_stage_set_title (CLUTTER_STAGE (stage), "I18n test");
  clutter_stage_set_user_resizable (CLUTTER_STAGE (stage), TRUE);
  clutter_actor_set_size (stage, STAGE_WIDTH, STAGE_HEIGHT);
  clutter_actor_set_reactive (stage, TRUE);
  clutter_actor_show (stage);

  label = clutter_text_new_full ("Serif Italic 40px",
                                 _("Testing internationalization"),
                                 CLUTTER_COLOR_Black);

  /* status bar occupies the top of window, so the text isn't shown
     when running on a target. It would be sufficient to draw the text
     100px below from the top. */
  clutter_actor_set_position (label, 20, 100);
  clutter_container_add_actor (CLUTTER_CONTAINER (stage), label);

  clutter_main ();

  return EXIT_SUCCESS;
}
