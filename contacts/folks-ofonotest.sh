#!/bin/sh

cur_dir=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
# Fix this hack by moving these tests to /usr/share/chaiwala-tests
common_dir=$(echo ${cur_dir} | sed -e s/lib/share/ -e s/contacts/common/)
. "${common_dir}/common.sh"
ensure_dbus_session

mkdir -p ~/.local/share/folks

export FOLKS_BACKEND_STORE_KEY_FILE_PATH="$cur_dir/ofonobackend.ini"
folks-inspect personas

