/*
 * Copyright (C) 2013 Collabora Ltd.
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Rodrigo Moya <rodrigo.moya@collabora.co.uk>
 *
 */

using Folks;
using Gee;

public class TestExtendedInfo : Object
{
  private GLib.MainLoop _main_loop;
  private int _result = -1;
  private IndividualAggregator _aggregator;
  private PersonaStore _primary_store = null;
  private BackendStore _backend_store = null;
  private Debug _debug = null;

  public TestExtendedInfo ()
    {
      this._main_loop = new GLib.MainLoop (null, false);
    }

  private void _cleanup (Individual individual)
    {
      GLib.print ("Removing Test Persona\n");
      this._aggregator.remove_individual.begin (individual, (s, r) =>
        {
          try
            {
              this._aggregator.remove_individual.end (r);
            }
          catch (GLib.Error e)
            {
              GLib.printerr ("Failed to remove persona: %s\n", e.message);
              this._debug.emit_print_status ();
            }

          this._main_loop.quit ();
        });
    }

  private void _individuals_changed_cb (MultiMap<Individual?, Individual?> changes)
    {
      foreach (var individual in changes.get_values ())
        {
          if (individual == null)
            continue;

          if (individual.full_name != "Test Persona")
            continue;

          GLib.print ("Adding an extended field\n");

          /* Add an extended field */
          individual.change_extended_field.begin ("X-FOLKS-EXTENDED-INFO", new ExtendedFieldDetails ("folks-test", null), (o, r) =>
            {
              try
                {
                  individual.change_extended_field.end (r);

                  ExtendedFieldDetails? efd = individual.get_extended_field ("X-FOLKS-EXTENDED-INFO");
                  if (efd != null)
                    {
                      if (efd.value == "folks-test")
                        {
                          this._result = 0;
                          this._cleanup (individual);
                        }
                      else
                        {
                          GLib.printerr ("Mismatched extended field value\n");
                          this._debug.emit_print_status ();
                          this._cleanup (individual);
                        }
                    }
                  else
                    {
                      GLib.printerr ("Extended field not added!!\n");
                      this._debug.emit_print_status ();
                      this._cleanup (individual);
                    }
                }
              catch (GLib.Error e)
                {
                  GLib.printerr ("Failed to change extended field: %s\n", e.message);
                  this._debug.emit_print_status ();
                  this._cleanup (individual);
                }
            });
        }
    }

  private void _store_prepared_cb (Object _pstore, ParamSpec ps)
    {
	  GLib.print ("Store prepared, adding new persona\n");

      HashTable<string, Value?> details = new HashTable<string, Value?> (str_hash, str_equal);
      details.insert (Folks.PersonaStore.detail_key (PersonaDetail.FULL_NAME), "Test Persona");

      this._aggregator.add_persona_from_details.begin (null, this._primary_store, details, (o, r) =>
        {
          try
            {
              var new_contact = this._aggregator.add_persona_from_details.end (r);
              if (new_contact == null)
                {
                  this._main_loop.quit ();
                }
            }
          catch (GLib.Error e)
            {
              GLib.printerr ("Failed to add persona: %s\n", e.message);
              this._debug.emit_print_status ();
              this._main_loop.quit ();
            }
        });
    }

  private void _run_extended_info_test ()
    {
      if (this._primary_store == null)
        {
		  foreach (var backend in this._backend_store.enabled_backends.values)
            {
              GLib.print ("Found backend %s\n", backend.name);
              if (backend.name == "eds")
                {
                  foreach (PersonaStore s in backend.persona_stores.values)
                    {
                      GLib.print ("Found primary store %s\n", s.display_name);
                      this._primary_store = backend.persona_stores.get (s.id);
					  this._primary_store.notify["is-prepared"].connect (this._store_prepared_cb);
                    }

				  break;
			    }
			}
		}
    }

  public int run ()
    {
      /* Load backends */
      this._debug = Debug.dup ();
	  this._backend_store = BackendStore.dup ();
	  this._backend_store.prepare.begin ((o, r) =>
		{
		  this._backend_store.prepare.end (r);
		});

	  this._aggregator = IndividualAggregator.dup_with_backend_store (this._backend_store);
	  this._aggregator.individuals_changed_detailed.connect (this._individuals_changed_cb);
	  this._aggregator.prepare.begin ((o, r) =>
		{
		  try
			{
			  this._aggregator.prepare.end (r);
			  _run_extended_info_test ();
			}
		  catch (GLib.Error e)
		    {
			  GLib.error ("Failed to prepare aggregator: %s\n", e.message);
			}
		});
   
	  this._main_loop.run ();

      return this._result;
    }
}

public int main (string[] args)
{
  int result;
  var test = new TestExtendedInfo ();

  result = test.run ();
  if (result == 0)
    GLib.print ("folks-extended-info: PASSED\n");
  else
    GLib.print ("folks-extended-info: FAILED\n");

  return result;
}
