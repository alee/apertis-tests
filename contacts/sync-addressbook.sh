#!/bin/sh

cur_dir=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
# Fix this hack by moving these tests to /usr/share/chaiwala-tests
common_dir=$(echo ${cur_dir} | sed -e s/lib/share/ -e s/contacts/common/)
. "${common_dir}/common.sh"
ensure_dbus_session

mac_addr=$1

if [ -z "$mac_addr" ]
then
  echo "usage synce-addressbook.sh phone_mac_address"
else

# create target-config
syncevolution --configure \
                syncURL= \
                addressbook/backend=pbap \
                addressbook/database=obex-bt://$mac_addr \
                target-config@pbap addressbook

# print items
#syncevolution --print-items \
#                target-config@pbap addressbook

# configure synchronization
syncevolution --configure \
                --template SyncEvolution_Client \
                --keyring=no \
                syncURL=local://@pbap \
                pbap

# run synchronization
syncevolution --sync refresh-from-client pbap addressbook

folks-inspect personas

fi

